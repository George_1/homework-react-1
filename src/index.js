import React from 'react';
import ReactDom from 'react-dom';

function Zodiacs () {
    return(
        <table border="3">        
        <tr>
            <td>
                Овен : 21 марта — 19 апреля
            </td>

            <td>
                Телец : 20 апреля — 20 мая	
            </td>
            
            <td>
                Близнецы : 21 мая — 20 июня
            </td>
        </tr>
        <tr>
            <td>
                   Рак : 21 июня — 22 июля
            </td>

            <td>
                Лев	: 23 июля — 22 августа
            </td>

            <td>
                Дева : 23 августа — 22 сентября
            </td>
        </tr>
        <tr>
            <td>
            	Весы : 23 сентября — 22 октября
            </td>
           
            <td>
                Скорпион : 23 октября — 21 ноября
            </td>

            <td>
            	Стрелец : 22 ноября — 21 декабря
            </td>
        </tr>
        <tr>
            <td>
            	Козерог : 22 декабря — 19 января
            </td>

            <td>
            	Водолей : 20 января — 18 февраля
            </td>

            <td>
            	Рыбы : 19 февраля — 20 марта
            </td>
        </tr>
</table>
    )
}

ReactDom.render(<Zodiacs></Zodiacs>, document.querySelector(".one"))
